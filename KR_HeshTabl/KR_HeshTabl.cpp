﻿#include <stdio.h>
#include <stdlib.h>

struct set
{
	int k;
	int dt;
};
struct set* array;
int c = 10;
int size = 0;
int HashFunk(int k)
{
	return (k % c);
}
void Insert(int k, int data)
{
	int indx = HashFunk(k);
	if (array[indx].dt == 0)
	{
		array[indx].k = k;
		array[indx].dt = data;
		size++;
		printf("Ключ ( %d ) наявний \n", k);
	}
	else if (array[indx].k == k)
	{
		array[indx].dt = data;
	}
	else
	{
		printf("Виникла Колізація\n");
	}
}
void return_elem(int k)
{
	int indx = HashFunk(k);
	if (array[indx].dt == 0)
	{
		printf("\n Даного ключа не існує!\n");
	}
	else
	{
		array[indx].k = 0;
		array[indx].dt = 0;
		size--;
		printf("Ключ ( %d ) видалено \n", k);
	}
}
int checkPr(int n)
{
	int i;
	if (n == 1 || n == 0)
	{
		return 0;
	}
	for (i = 2; i < n / 2; i++)
	{
		if (n % i == 0)
		{
			return 0;
		}
	}
	return 1;
}
int GetPr(int n)
{
	if (n % 2 == 0)
	{
		n++;
	}
	while (!checkPr(n))
	{
		n += 2;
	}
	return n;
}
void ArrInit()
{
	c = GetPr(c);
	array = (struct set*)malloc(c * sizeof(struct set));
	for (int i = 0; i < c; i++)
	{
		array[i].k = 0;
		array[i].dt = 0;
	}
}
int size_of_hashtbl()
{
	return size;
}
void display()
{
	int i;
	for (i = 0; i < c; i++)
	{
		if (array[i].dt == 0)
		{
			printf("array[%2d]: ~~ \n", i);
		}
		else
		{
			printf("Ключ: %d array[%d]: %d \n", array[i].k, i, array[i].dt);
		}
	}
}
int main()
{
	system("chcp 1251");
	system("cls");
	int richenna, key, dani, n;
	int c = 0;
	ArrInit();

	do
	{
		printf("\n\n");
		printf("~~~~~~~~~~~~~~ Menu ~~~~~~~~~~~~~~\n");
		printf("1 - Вставити елемент в хеш-таблицю\n");
		printf("2 - Видалити елемент з хеш-таблиці\n");
		printf("3 - Розмір хеш-таблиці\n");
		printf("4 - Вивести хеш-таблицю\n");
		printf("0 - Вихід\n");
		printf("--> ");
		scanf_s("%d", &richenna);
		switch (richenna)
		{
		case 0: {
			return 0;
		}break;
		case 1: {
			printf("Введіть ключ: \t");
			scanf_s("%d", &key);
			printf("Введіть данні: \t");
			scanf_s("%d", &dani);
			Insert(key, dani);
		}break;
		case 2: {
			printf("Введіть ключ, який хочете видалити: \t");
			scanf_s("%d", &key);
			return_elem(key);
		}break;
		case 3: {
			n = size_of_hashtbl();
			printf("Розмір хеш-таблиці - %d\n", n);
		}break;
		case 4: {
			display();
		}break;
		default:
			break;
		}
	} while (true);
	return 0;
}